    </div>
    <!-- end #main-wrapper -->
    <footer>
        <!-- begin copyright -->
		<?php echo of_get_option('footer_copyright')  ?>
		<!-- end copyright -->

		<!-- Site5 Credits-->
		<br>Created by <a href="http://www.dharmeshvasani.in/">Dharmesh Vasani</a>. Experts in Web Apps Development</a>
		<!-- end Site5 Credits-->
    </footer>

	<?php wp_footer(); ?>

	</body>
</html>